<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-0 rounded-5">
            <div class="modal-header border-0 justify-content-end">
                <button type="button" class="modal_btn_close" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></button>
            </div>
            <div class="modal-body p-lg-5 p-4 pt-0 pt-lg-0">
                <div class="row align-items-center">
                    <div class="col-md-5 mx-auto d-none d-md-block">
                        <img src="../assets/images/log_side.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-md-6">
                        <form action="dashboard.php">
                            <h4 class="fs40 oswaldff mb-lg-5 mb-3">Log In</h4>
                            <label for="">Email</label>
                            <div class="inputGroup mb-4">
                                <input type="email" class="inputControl bg-white border text-black py-2 px-3 rounded-2" placeholder="Enter Email" style="height: 50px;">
                            </div>
                            <label for="">Password</label>
                            <div class="inputGroup mb-4">
                                <input type="text" class="inputControl bg-white border text-black py-2 px-3 rounded-2" placeholder="Enter Password" style="height: 50px;">
                            </div>
                            <div class="pt-4 text-center">
                                <button type="submit" class="theme_btn bg_lightTheme w-auto px-5 rounded-3 fw-semibold">Log In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>