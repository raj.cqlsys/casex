// image upload
function loadFile(event, c) {
    var image = document.getElementById(c);
    image.src = URL.createObjectURL(event.target.files[0]);
}
// image upload

// back btn
function goback() {
    history.back();
};
// \ back btn

// aspect ratio
function setDivHeight() {
    // Get all divs with data-aspect attribute
    const divs = document.querySelectorAll('[data-aspect]');

    divs.forEach(div => {
        // Split the aspect ratio into width and height
        const [width, height] = div.getAttribute('data-aspect').split('/');

        // Calculate the height based on the width and aspect ratio
        const calculatedHeight = div.offsetWidth * (height / width);
        console.log(calculatedHeight);
        // Set the height of the div
        div.style.height = `${calculatedHeight}px`;
    });
}

// Run the function on window load and resize
window.addEventListener('resize', setDivHeight);
window.addEventListener('load', setDivHeight);
window.addEventListener('load', () => {
    // Wait for 0.5 seconds before running setDivHeight
    setTimeout(() => {
        // Run setDivHeight every 0.5 seconds
        setInterval(setDivHeight, 500); // 500milliseconds = 0.5 seconds
    }, 500); // 500milliseconds = 0.5 seconds
});
// \ aspect ratio

function chatMenu() {
    var mcvar = document.getElementById("patinfoLeft");
    mcvar.classList.add("opne_list");
}
function chatMenuOpen() {
    var mcvar = document.getElementById("patinfoLeft");
    mcvar.classList.remove("opne_list");
}