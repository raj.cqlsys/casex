<div class="top_navbar">
    <div class="container d-flex justify-content-end gap-3 align-items-center">
        <a href="" class="nt_bell_ico"><i class="ph-fill ph-bell fa-shake"></i> <span></span></a>
        <button type="button" class="theme_btn w-auto px-4" data-bs-toggle="modal" data-bs-target="#loginModal">Login</button>
    </div>
</div>
<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand" href="index.php"><img src="../assets/images/logo.png" alt=""></a>
        <button class="navbar-toggler border-0 shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="ph ph-list text-white fs-3"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">FAQ'S</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact Us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>