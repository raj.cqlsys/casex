<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="icon" type="images/x-icon" href="../assets/images/favicon.png">
    <!--  ajax -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.css">
    <!-- bootstrap 5.3 -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <!-- swiper slider -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />
    <!-- phosphor icons -->
    <link rel="stylesheet" href="../assets/Fonts/fill/style.css">
    <link rel="stylesheet" href="../assets/Fonts/duotone/style.css">
    <link rel="stylesheet" href="../assets/Fonts/bold/style.css">
    <link rel="stylesheet" href="../assets/Fonts/regular/style.css">
    <!-- font-awesome icon file -->
    <script src="https://kit.fontawesome.com/9afdb21cde.js" crossorigin="anonymous"></script>
    <!-- my style css file -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/style_raj.css">
</head>

<body>
    <?php
    function setPageTitle($title)
    {
        echo "<script>document.title = '$title';</script>";
    }
    ?>