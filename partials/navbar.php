<div class="top_navbar">
    <div class="container d-flex justify-content-end gap-3 align-items-center">
        <a href="notifications.php" class="nt_bell_ico"><i class="ph-fill ph-bell fa-shake"></i> <span></span></a>

        <button class="user_dorp_btn" data-bs-toggle="modal" data-bs-target="#logoutModal" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="../assets/images/u2.png" alt="">
            <div class="text-start">
                <span>John Maker</span>
                <small>Logout</small>
            </div>
        </button>
    </div>
</div>
<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand" href="index.php"><img src="../assets/images/logo.png" alt=""></a>
        <button class="navbar-toggler border-0 shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="ph ph-list text-white fs-3"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="dashboard.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about.php">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="faq.php">FAQ'S</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact.php">Contact Us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>