<footer class="py-5">
    <div class="container">
        <div class="text-center">
            <a href="index.php" class="footer_brand mb-5">
                <img src="../assets/images/logo.png" alt="">
            </a>
            <div class="d-flex align-items-center justify-content-center gap-2">
                <a href="terms.php" class="foot_link">Terms of Use</a>
                <span class="foot_link">|</span>
                <a href="privacy.php" class="foot_link">Privacy Policy</a>
            </div>
        </div>
        <hr class="my-lg-5 my-3">
        <p class="foot_link mb-0 text-center">© 2024, Case X Manager. All Rights Reserved.</p>
    </div>
</footer>

<div class="modal fade" id="logoutModal" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content rounded-4">
            <div class="modal-body py-4">
                <div class="logout_modal">
                    <div class="text-center">
                        <i class="ph ph-sign-out modal_icon"></i>
                    </div>
                    <h4>Logout</h4>
                    <p class="px-lg-4">Are you sure you want to logout this account?</p>
                    <div class="modal_btn_group">
                        <a href="index.php" class="theme_btn">yes</a>
                        <button type="button" class="bg-secondary-subtle text-black" data-bs-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include '../modals/loginModal.php'; ?>