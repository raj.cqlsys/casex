<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>
<!--  ajax -->

<?php include '../partials/navbar.php'; ?>
<section class="py-5 bg_dark">
    <div class="container py-lg-4">
        <div class="row">
            <div class="col-lg-9  text-white mx-auto">
                <h3 class="fs32 under_wave text-center">Your Law Firm Detail <i class="ph ph-pulse under_wave_item"></i></h3>
                <div class="mt-5 bg_black p-5 rounded-4 ">
                    <div class="row">
                        <div class="col-md-9 mx-auto">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="" class="fs16 mb-2 text-dark bg_grdnt p-3 rounded-3 d-flex gap-3" style="white-space: nowrap;">Law Firm Name
                                        <small class="ms-auto opacity-75 text-end d-block" style="white-space: normal;">Abc Firm</small>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <label for="" class="fs16 mb-2 text-dark bg_grdnt p-3 rounded-3 d-flex gap-3" style="white-space: nowrap;">Website
                                        <small class="ms-auto opacity-75 text-end text-danger d-block" style="white-space: normal;">  wwwcasexmanager.Com</small>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <label for="" class="fs16 mb-2 text-dark bg_grdnt p-3 rounded-3 d-flex gap-3" style="white-space: nowrap;">Phone Number
                                        <small class="ms-auto opacity-75 text-end d-block" style="white-space: normal;"> +65 2326548563</small>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <label for="" class="fs16 mb-2 text-dark bg_grdnt p-3 rounded-3 d-flex gap-3" style="white-space: nowrap;">Address 1
                                        <small class="ms-auto opacity-75 text-end d-block" style="white-space: normal;"> F-267, Phase 8B, Industrial Area, Sector 74   </small>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <label for="" class="fs16 mb-2 text-dark bg_grdnt p-3 rounded-3 d-flex gap-3" style="white-space: nowrap;">Address 2
                                        <small class="ms-auto opacity-75 text-end d-block" style="white-space: normal;"> F-266 Phase 7B, Industrial Area, Sector 74a</small>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <label for="" class="fs16 mb-2 text-dark bg_grdnt p-3 rounded-3 d-flex gap-3" style="white-space: nowrap;">Address 3
                                        <small class="ms-auto opacity-75 text-end d-block" style="white-space: normal;"> F-549(P) Ground Floor Phase 8-A, Industrial Area, Sector 75 SAS Nagar</small>
                                    </label>
                                </div>
                            

                                <div class="col-12 text-center d-flex justify-content-evenly">
                                    <button type="submit" class="theme_btn w-auto px-5 rounded-3 mt-4">Edit</button>
                                    <button type="submit" class=" bg-danger border-0 text-white w-auto px-5 rounded-3 mt-4">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>