<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>
<!--  ajax -->

<?php include '../partials/navbar.php'; ?>
<section class="py-5">
    <div class="container py-lg-4">
        <div class="row">
            <div class="col-xxl-9 col-md-11 text-white mx-auto">
                <h3 class="fs32 under_wave text-center">Dashboard <i class="ph ph-pulse under_wave_item"></i></h3>
                <div class="mt-5 border_white bg-dark p-5 rounded-4 h-100">
                    <div class="row gy-4">
                        <div class="col-md-4">
                            <div class="dashborad bg_grdnt text-center py-5 px-3 rounded-4 position-relative">
                                <a href="law_firm_details.php" class="text-dark text-decoration-none d-inline-block w-100 mb-5">
                                    <img src="../assets/images/d0.png" alt="">
                                    <h3 class="mb-3 fs24">Your Law Firm</h3>
                                </a>
                                <a href="add_new_firm.php" class="theme_btn w-auto px-3 rounded-3 mx-auto mt-4 position-absolute law_frm text-white oswaldff">Add New Law Firm</a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dashborad bg_grdnt text-center py-5 px-3 rounded-4 position-relative">
                                <a href="patient_info.php" class="text-dark text-decoration-none d-inline-block w-100 mb-5">
                                    <img src="../assets/images/d2.png" alt="">
                                    <h3 class="mb-3 fs24">Your Patients</h3>
                                </a>
                                <a href="#" class="theme_btn w-auto px-3 rounded-3 mx-auto mt-4 position-absolute law_frm text-white oswaldff">Add New User</a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dashborad bg_grdnt text-center py-5 px-3 rounded-4 position-relative">
                                <a href="" class="text-dark text-decoration-none d-inline-block w-100 mb-5">
                                    <img src="../assets/images/d3.png" alt="">
                                    <h3 class="mb-3 fs24">Your Message</h3>
                                </a>
                                <a href="#" class="theme_btn w-auto px-3 rounded-3 mx-auto mt-4 position-absolute law_frm text-white oswaldff">View Your Message</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>