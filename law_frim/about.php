<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>
<!--  ajax -->

<?php include '../partials/navbar.php'; ?>


<section class="py-5 bg_grdnt text-center">
    <div class="container">
        <div class="row pt-lg-5 justify-content-between align-items-center">
            <div class="col-md-7 mx-auto">
                <div class="row align-items-center ">
                <div class="col-lg-12">
                    <h3 class="fs75 mb-4 text-dark oswaldff">About</h3>
                    <p class="fs30 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    </p>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<img src="../assets/images/about_banner.png" class="img-fluid w-100" alt="">
<section class="py-5 bg-dark">
    <div class="container">
        <div class="row pt-lg-5 justify-content-between align-items-center">
            <div class="col-md-10 mx-auto">
                <div class="row align-items-center">
                <div class="col-lg-6">
                    <h3 class="fs75 mb-4">Careers</h3>
                    <p class="fs30 text-white">Lorem ipsum dolor sit amet,
                        onsectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar a ibus leo.
                    </p>
                </div>
                <div class="col-lg-6">
                    <img src="../assets/images/about.svg" class="img-fluid" alt="">
                </div>
            </div>
            </div>
        </div>
    </div>
</section>




<section class="py-5 ">
    <div class="container py-lg-4">
        <div class="row">
            <div class="col-lg-6 text-center text-white mx-auto">
                <h3 class="fs32 under_wave">Contact Us <i class="ph ph-pulse under_wave_item"></i></h3>
                <p class="fs24">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar. Lorem ipsum dolor sit amet</p>
            </div>
        </div>
        <div class="row pt-5 gy-4">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="contact_card">
                    <div class="cont_ca_ico">
                        <i class="ph ph-map-pin"></i>
                    </div>
                    <div class="cont_ca_body">
                        <h4 class="fs24 text-white fw-semibold">LOCATION</h4>
                        <p class="fs24 text-white fw-normal mb-0">Lorem Ipsum is simply text.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <a href="mailto:info@demo.com" class="contact_card">
                    <div class="cont_ca_ico">
                        <i class="ph ph-envelope-simple"></i>
                    </div>
                    <div class="cont_ca_body">
                        <h4 class="fs24 text-white fw-semibold">EMAIL</h4>
                        <p class="fs24 text-white fw-normal mb-0">info@demo.com</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <a href="tel:+1 123-445-7777" class="contact_card">
                    <div class="cont_ca_ico">
                        <i class="ph ph-phone-call"></i>
                    </div>
                    <div class="cont_ca_body">
                        <h4 class="fs24 text-white fw-semibold">PHONE</h4>
                        <p class="fs24 text-white fw-normal mb-0">(+1) 123-445-7777</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>


<section class="py-5 bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="fs90 oswaldff text_theme">Discover Our Suite Of Solutions</h2>
            </div>
        </div>
        <div class="row pt-lg-5 justify-content-between align-items-center  gy-4">
            <div class="col-lg-5">
                <img src="../assets/images/f2.png" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6">
                <h3 class="fs75 mb-4">Seamless Operations for Funders</h3>
                <p class="fs30 text_down_white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                </p>
            </div>
        </div>
    </div>
</section>
<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>