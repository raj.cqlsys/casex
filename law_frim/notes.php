<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>


<?php include '../partials/navbar.php'; ?>
<section class=" py-5 bg_dark">
    <div class="container py-lg-4">
        <h3 class="fs32 under_wave text-center text-white">Patient Info <i class="ph ph-pulse under_wave_item"></i></h3>


        <div class="patient_info_view mt-5" id="patinfoLeft">
            <?php include './patient_infe_list.php'; ?>
            <div class="right_panal">
                <i class="ph ph-arrow-left fs-4 text-white d-xl-none" style="cursor: pointer;" onclick="chatMenuOpen()"></i>
                <div class="rpanal_head">
                    <div class="row">
                        <div class="col-md-2 text-md-start text-center mb-md-0 mb-3">
                            <img src="../assets/images/u2.png" class="img-fluid w-75" style="max-width: 250px;" alt="">
                        </div>
                        <div class="col-md-5">
                            <h5 class="fs18 text-white fw-semibold">Amit Deo</h5>
                            <p class="fs14 text-white fw-light mb-1">Email: casexmanager@gmail.com</p>
                            <p class="fs14 text-white fw-light mb-1">Phone Number: +1 123 456 7890</p>
                            <p class="fs14 text-white fw-light mb-1">Role: Patient</p>
                            <p class="fs14 text-white fw-light mb-1">Visit Type: Urgent</p>
                            <p class="fs14 text-white fw-light mb-1">Address: 1 Main Street, Austin, TX, 65654</p>
                        </div>
                        <div class="col-md-5">
                            <h5 class="fs18 text-white fw-semibold d-md-block d-none">&nbsp;</h5>
                            <p class="fs14 text-white fw-light mb-1">Age: 34</p>
                            <p class="fs14 text-white fw-light mb-1">Birth Date: 02/20/1987</p>
                            <p class="fs14 text-white fw-light mb-1">Provider: John Marker</p>
                            <p class="fs14 text-white fw-light mb-1">Specialty: Ortho</p>
                        </div>
                    </div>
                    <div class="patient_tab">
                        <a href="patient_info.php" class="patab_link">All</a>
                        <a href="medical_records.php" class="patab_link">Medical Records</a>
                        <a href="insurance.php" class="patab_link">Insurance</a>
                        <a href="legal.php" class="patab_link">Legal</a>
                        <a href="notes.php" class="patab_link active">Notes</a>
                    </div>
                </div>

                <div class="rpanal_body">
                    <div class="p-4 bg_black border border-white rounded-4">
                        <div class="miln_body">
                            <p class="fs14 mb-1 text-white"><small>12 Sep 2023</small></p>
                            <div class="pdt_card mb-2">
                                <a href="note_detail.php" class="text-decoration-none text-black fs14 fw-normal" style="white-space: normal;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt commodi, debitis nemo quod autem delectus modi nihil enim omnis sit reprehenderit velit obcaecati, doloremque hic officia! Accusamus fugiat dolores ipsum. Quas, deserunt aliquid nihil ut soluta modi vero dolorem laudantium quibusdam, consectetur est. Aspernatur, perferendis.</a>
                            </div>
                            <p class="fs14 mb-1 text-white"><small>12 Sep 2023</small></p>
                            <div class="pdt_card mb-2">
                                <a href="note_detail.php" class="text-decoration-none text-black fs14 fw-normal" style="white-space: normal;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt commodi, debitis nemo quod autem delectus modi nihil enim omnis sit reprehenderit velit obcaecati, doloremque hic officia! Accusamus fugiat dolores ipsum. Quas, deserunt aliquid nihil ut soluta modi vero dolorem laudantium quibusdam, consectetur est. Aspernatur, perferendis.</a>
                            </div>
                            <p class="fs14 mb-1 text-white"><small>12 Sep 2023</small></p>
                            <div class="pdt_card mb-2">
                                <a href="note_detail.php" class="text-decoration-none text-black fs14 fw-normal" style="white-space: normal;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt commodi, debitis nemo quod autem delectus modi nihil enim omnis sit reprehenderit velit obcaecati, doloremque hic officia! Accusamus fugiat dolores ipsum. Quas, deserunt aliquid nihil ut soluta modi vero dolorem laudantium quibusdam, consectetur est. Aspernatur, perferendis.</a>
                            </div>
                            <p class="fs14 mb-1 text-white"><small>12 Sep 2023</small></p>
                            <div class="pdt_card mb-2">
                                <a href="note_detail.php" class="text-decoration-none text-black fs14 fw-normal" style="white-space: normal;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt commodi, debitis nemo quod autem delectus modi nihil enim omnis sit reprehenderit velit obcaecati, doloremque hic officia! Accusamus fugiat dolores ipsum. Quas, deserunt aliquid nihil ut soluta modi vero dolorem laudantium quibusdam, consectetur est. Aspernatur, perferendis.</a>
                            </div>
                        </div>

                        <div class="mt-4 text-end">
                            <a href="add_note.php" class="theme_btn ms-auto px-5 rounded-4" style="width: fit-content;">Add Note</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>