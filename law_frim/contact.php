<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>
<!--  ajax -->
<style>
    .iti__selected-dial-code {
	color: #000;
}
</style>
<?php include '../partials/navbar.php'; ?>
<section class="py-5 bg_dark">
    <div class="container py-lg-4">
        <div class="row">
        <div class="col-lg-6 text-center text-white mx-auto">
                <h3 class="fs32">Contact Us </h3>
                <p class="fs18 mb-3 mt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since then printer took a galley of type.</p>
            </div>
            <div class="col-lg-7 col-md-10 mx-auto mt-5">
                <form action="">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Name</label>
                            <div class="inputGroup mb-3"><input  type="text" name="" id="" class="inputControl bg-white text-dark" placeholder="Enter Name"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Email</label>
                            <div class="inputGroup mb-3"><input type="email" name="" id="" class="inputControl bg-white text-dark" placeholder="Enter Email"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Phone Number</label>
                            <div class="inputGroup mb-3"><input id="mobile_code" type="text" name="" id="" class="inputControl bg-white text-dark" placeholder="Enter Phone Number"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Subject</label>
                            <div class="inputGroup mb-3"><input type="email" name="" id="" class="inputControl bg-white text-dark" placeholder="Enter Subject"></div>
                        </div>
                        <div class="col-12">
                            <label for="" class="fs18 mb-2 text-white">Message</label>
                            <div class="inputGroup mb-3">
                                <textarea name="" id="" placeholder="Write here..." class="inputControl bg-white text-dark h-auto pt-4" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="theme_btn w-auto px-5 rounded-3 mx-auto mt-4">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d805170.0496601658!2d145.05313529999998!3d-37.9725665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad646b5d2ba4df7%3A0x4045675218ccd90!2sMelbourne%20VIC%2C%20Australia!5e0!3m2!1sen!2sin!4v1716285172913!5m2!1sen!2sin"  style="width:100%;border:0; height:500px" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>