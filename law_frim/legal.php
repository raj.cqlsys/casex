<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>


<?php include '../partials/navbar.php'; ?>
<section class=" py-5 bg_dark">
    <div class="container py-lg-4">
        <h3 class="fs32 under_wave text-center text-white">Patient Info <i class="ph ph-pulse under_wave_item"></i></h3>


        <div class="patient_info_view mt-5" id="patinfoLeft">
            <?php include './patient_infe_list.php'; ?>
            <div class="right_panal">
                <i class="ph ph-arrow-left fs-4 text-white d-xl-none" style="cursor: pointer;" onclick="chatMenuOpen()"></i>
                <div class="rpanal_head">
                    <div class="row">
                        <div class="col-md-2 text-md-start text-center mb-md-0 mb-3">
                            <img src="../assets/images/u2.png" class="img-fluid w-75" style="max-width: 250px;" alt="">
                        </div>
                        <div class="col-md-5">
                            <h5 class="fs18 text-white fw-semibold">Amit Deo</h5>
                            <p class="fs14 text-white fw-light mb-1">Email: casexmanager@gmail.com</p>
                            <p class="fs14 text-white fw-light mb-1">Phone Number: +1 123 456 7890</p>
                            <p class="fs14 text-white fw-light mb-1">Role: Patient</p>
                            <p class="fs14 text-white fw-light mb-1">Visit Type: Urgent</p>
                            <p class="fs14 text-white fw-light mb-1">Address: 1 Main Street, Austin, TX, 65654</p>
                        </div>
                        <div class="col-md-5">
                            <h5 class="fs18 text-white fw-semibold d-md-block d-none">&nbsp;</h5>
                            <p class="fs14 text-white fw-light mb-1">Age: 34</p>
                            <p class="fs14 text-white fw-light mb-1">Birth Date: 02/20/1987</p>
                            <p class="fs14 text-white fw-light mb-1">Provider: John Marker</p>
                            <p class="fs14 text-white fw-light mb-1">Specialty: Ortho</p>
                        </div>
                    </div>

                    <div class="patient_tab">
                        <a href="patient_info.php" class="patab_link">All</a>
                        <a href="medical_records.php" class="patab_link">Medical Records</a>
                        <a href="insurance.php" class="patab_link">Insurance</a>
                        <a href="legal.php" class="patab_link active">Legal</a>
                        <a href="notes.php" class="patab_link">Notes</a>
                    </div>
                </div>

                <div class="rpanal_body">
                    <div class="p-4 bg_black border border-white rounded-4">
                        <h4 class="fs18 text-white">Legal Information</h4>
                        <p class="fs14 fw-normal text-white opacity-75">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply.</p>
                        <div class="row g-3 w-100 mx-0">
                            <div class="col-sm-6">
                                <div class="leg_select">
                                    <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                        Police Report
                                    </a>
                                    <i class="ph-fill ph-caret-down"></i>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="leg_select">
                                    <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                        Hipaa
                                    </a>
                                    <i class="ph-fill ph-caret-down"></i>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="leg_select">
                                    <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                        Compliance Agreements
                                    </a>
                                    <i class="ph-fill ph-caret-down"></i>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="leg_select">
                                    <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                        Lorem Ipsum Report 1
                                    </a>
                                    <i class="ph-fill ph-caret-down"></i>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="leg_select">
                                    <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                        Lorem Ipsum Report 2
                                    </a>
                                    <i class="ph-fill ph-caret-down"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>