<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>
<!--  ajax -->

<?php include '../partials/navbar.php'; ?>
<section class="py-5">
    <div class="container py-lg-4">
        <div class="row">
            <div class="col-lg-9  text-white mx-auto">
                <h3 class="fs32 under_wave text-center">Add New Law Firm <i class="ph ph-pulse under_wave_item"></i></h3>
                <div class="mt-5 bg-dark p-5 rounded-4 border_white">
                    <div class="row">
                        <div class="col-md-9 mx-auto">
                            <form action="law_firm_details.php">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="" class="fs18 mb-2 text-white">Law Firm Name</label>
                                        <div class="inputGroup mb-3"><input type="text" name="" id="" class="inputControl bg_grdnt text-dark" placeholder="Enter Firm name"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" class="fs18 mb-2 text-white">Website</label>
                                        <div class="inputGroup mb-3"><input type="text" name="" id="" class="inputControl bg_grdnt text-dark" placeholder="Enter website "></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" class="fs18 mb-2 text-white">Phone Number</label>
                                        <div class="inputGroup mb-3"><input type="text" name="" id="" class="inputControl bg_grdnt text-dark" placeholder="Enter Phone Number "></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" class="fs18 mb-2 text-white">Address 1</label>
                                        <div class="inputGroup mb-3"><input type="text" name="" id="" class="inputControl bg_grdnt text-dark" placeholder="Enter Address "></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" class="fs18 mb-2 text-white">Address 2</label>
                                        <div class="inputGroup mb-3"><input type="text" name="" id="" class="inputControl bg_grdnt text-dark" placeholder="Enter Address "></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" class="fs18 mb-2 text-white">Address 3</label>
                                        <div class="inputGroup mb-3"><input type="text" name="" id="" class="inputControl bg_grdnt text-dark" placeholder="Enter Address "></div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <button type="submit" class="theme_btn w-auto px-5 rounded-3 mx-auto mt-4">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>