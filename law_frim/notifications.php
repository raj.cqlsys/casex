<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>
<!--  ajax -->

<?php include '../partials/navbar.php'; ?>
<section class="py-5">
    <div class="container py-lg-4">
        <div class="row">
            <div class="col-lg-9  text-white mx-auto">
                <h3 class="fs32 under_wave text-center">Notifications <i class="ph ph-pulse under_wave_item"></i></h3>
                <div class="mt-5">
                    <div class="row bg_grdnt rounded-3 border_white mb-3">
                        <div class="col-md-1 col-sm-3 col-4 mx-auto p-3">
                            <img src="../assets/images/noti.png" alt="" class="w-100 rounded">
                        </div>
                        <div class="col-md-11 text-dark p-3">
                            <div class="noti">
                                <h4 class="d-flex"> <span class="oswaldff fs18"> A new patient is added law firm.</span>   <small class="ms-auto fs16">2 days ago</small></h4>
                                <p class="mb-0">Lorem ipsum dolor  sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Turpis egestas sed tempus urna et pulvinar semleko.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row bg_grdnt rounded-3 border_white mb-3">
                        <div class="col-md-1 col-sm-3 col-4 mx-auto p-3">
                            <img src="../assets/images/noti.png" alt="" class="w-100 rounded">
                        </div>
                        <div class="col-md-11 text-dark p-3">
                            <div class="noti">
                                <h4 class="d-flex"> <span class="oswaldff fs18"> A new patient is added law firm.</span>   <small class="ms-auto fs16">2 days ago</small></h4>
                                <p class="mb-0">Lorem ipsum dolor  sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Turpis egestas sed tempus urna et pulvinar semleko.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row bg_grdnt rounded-3 border_white mb-3">
                        <div class="col-md-1 col-sm-3 col-4 mx-auto p-3">
                            <img src="../assets/images/noti.png" alt="" class="w-100 rounded">
                        </div>
                        <div class="col-md-11 text-dark p-3">
                            <div class="noti">
                                <h4 class="d-flex"> <span class="oswaldff fs18"> A new patient is added law firm.</span>   <small class="ms-auto fs16">2 days ago</small></h4>
                                <p class="mb-0">Lorem ipsum dolor  sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Turpis egestas sed tempus urna et pulvinar semleko.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row bg_grdnt rounded-3 border_white mb-3">
                        <div class="col-md-1 col-sm-3 col-4 mx-auto p-3">
                            <img src="../assets/images/noti.png" alt="" class="w-100 rounded">
                        </div>
                        <div class="col-md-11 text-dark p-3">
                            <div class="noti">
                                <h4 class="d-flex"> <span class="oswaldff fs18"> A new patient is added law firm.</span>   <small class="ms-auto fs16">2 days ago</small></h4>
                                <p class="mb-0">Lorem ipsum dolor  sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Turpis egestas sed tempus urna et pulvinar semleko.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row bg_grdnt rounded-3 border_white mb-3">
                        <div class="col-md-1 col-sm-3 col-4 mx-auto p-3">
                            <img src="../assets/images/noti.png" alt="" class="w-100 rounded">
                        </div>
                        <div class="col-md-11 text-dark p-3">
                            <div class="noti">
                                <h4 class="d-flex"> <span class="oswaldff fs18"> A new patient is added law firm.</span>   <small class="ms-auto fs16">2 days ago</small></h4>
                                <p class="mb-0">Lorem ipsum dolor  sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Turpis egestas sed tempus urna et pulvinar semleko.</p>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>

    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>