<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>


<?php include '../partials/navbar.php'; ?>
<section class=" py-5 bg_dark">
    <div class="container py-lg-4">
        <h3 class="fs32 under_wave text-center text-white"> <button type="button" class="back_btn_round position-absolute" onclick="goback()"><i class="ph ph-arrow-left"></i></button> Legal Detail <i class="ph ph-pulse under_wave_item"></i></h3>

        <h4 class="fs20 fw-medium text-white mb-2 mt-4">Legal Information</h4>
        <p class="fs14 fw-normal text-white mb-lg-5 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply.</p>

        <div class="accordion accordion-flush dark_accor" id="accordionFlushExample">
            <div class="accordion-item">
                <h2 class="accordion-header">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                        Police Report <i class="ph-fill ph-caret-down"></i>
                    </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse show" data-bs-parent="#accordionFlushExample">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. </p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply.</p>

                    <img src="../assets/images/pr.png" width="127" alt="">

                    <div class="row g-3 w-100 mx-0 mt-4">
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="leg_select">
                                <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                    Hipaa
                                </a>
                                <i class="ph-fill ph-caret-down"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="leg_select">
                                <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                    Compliance Agreements
                                </a>
                                <i class="ph-fill ph-caret-down"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="leg_select">
                                <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                    Lorem Ipsum Report 1
                                </a>
                                <i class="ph-fill ph-caret-down"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="leg_select">
                                <a href="legal_detail.php" class="py-2 fs14" name="" id="">
                                    Lorem Ipsum Report 2
                                </a>
                                <i class="ph-fill ph-caret-down"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>