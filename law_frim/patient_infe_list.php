<div class="patinfo_left" >
    <div class="pt_infoL_head">
        <div class="search_list">
            <input type="text" placeholder="Search for patient">
            <button type="submit"><i class="ph ph-magnifying-glass"></i></button>
        </div>
        <p class="fs14 mt-4 mb-0 text-white d-flex gap-2 align-items-center">
            <i class="fa-solid fa-arrow-down-short-wide fs18"></i> A-Z
        </p>
    </div>
    <div class="pt_infoL_body">
        <a href="#" onclick="chatMenu()" class="ptinL_link active">
            <h5>Amit Deo</h5>
            <span>DOB: 04/20/85 | DOL: 07/12/24</span>
            <span>Status: Demand sent</span>
        </a>
        <a href="#" onclick="chatMenu()" class="ptinL_link">
            <h5>Aadee</h5>
            <span>DOB: 04/20/85 | DOL: 07/12/11</span>
            <span>Status: Case settled - not yet disbursed</span>
        </a>
        <a href="#" onclick="chatMenu()" class="ptinL_link">
            <h5>Bruce Bowen</h5>
            <span>DOB: 04/20/85 | DOL: 07/12/22</span>
            <span>Status: Case settled - not yet disbursed</span>
        </a>
        <a href="#" onclick="chatMenu()" class="ptinL_link">
            <h5>Bruce Bowen</h5>
            <span>DOB: 04/20/85 | DOL: 07/12/14</span>
            <span>Status: Mediation or arbitration scheduled</span>
        </a>
        <a href="#" onclick="chatMenu()" class="ptinL_link">
            <h5>Charles Gilbert</h5>
            <span>DOB: 04/20/85 | DOL: 07/12/18</span>
            <span>Status: In Litigation</span>
        </a>
        <a href="#" onclick="chatMenu()" class="ptinL_link">
            <h5>Donna Smith</h5>
            <span>DOB: 04/20/85 | DOL: 07/12/16</span>
            <span>Status: Trial scheduled</span>
        </a>

    </div>
</div>