<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>


<?php include '../partials/navbar.php'; ?>
<section class=" py-5 bg_dark">
    <div class="container py-lg-4">
        <h3 class="fs32 under_wave text-center text-white"> <button type="button" class="back_btn_round position-absolute" onclick="goback()"><i class="ph ph-arrow-left"></i></button> Insurance Detail <i class="ph ph-pulse under_wave_item"></i></h3>
        <div class="row">
            <div class="col-md-4">
                <img src="../assets/images/in6.png" class="img-fluid" alt="">
            </div>
            <div class="col-md-8">
                <h4 class="fs16 text-white fw-semibold">MIMO Insurance</h4>
                <p class="text_down_white fz12"><i class="ph-fill ph-map-pin"></i> #3 Newbridge Court Chino Hills, CA 91709, United States</p>

                <div class="d-flex flex-wrap justify-content-start align-items-start gap-3">
                    <div class="ind_ctact">
                        <div class="ind_ctact_ico">
                            <i class="ph-bold ph-calendar-blank"></i>
                        </div>
                        <div class="ind_ctact_body">
                            <h6>Date</h6>
                            <span>12 Sep 2023</span>
                        </div>
                    </div>
                    <div class="ind_ctact">
                        <div class="ind_ctact_ico">
                            <i class="ph-bold ph-clock"></i>
                        </div>
                        <div class="ind_ctact_body">
                            <h6>Time</h6>
                            <span>10:00am</span>
                        </div>
                    </div>
                    <div class="ind_ctact">
                        <div class="ind_ctact_ico">
                            <i class="ph-fill ph-phone-call"></i>
                        </div>
                        <div class="ind_ctact_body">
                            <h6>Contact Number</h6>
                            <span>+1 123 456 7890</span>
                        </div>
                    </div>
                </div>

                <p class="fs14 text-white mt-4 lh-lg fw-normal">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply.
                </p>
            </div>
            <!-- col-end -->
            <div class="col-12">
                <h4 class="fs16 fw-semibold text_theme mt-4">Benefits</h4>
                <ul class="text-white ps-3">
                    <li class="fs14 text-white mt-4 lh-lg fw-normal">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    <li class="fs14 text-white mt-4 lh-lg fw-normal">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    <li class="fs14 text-white mt-4 lh-lg fw-normal">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    <li class="fs14 text-white mt-4 lh-lg fw-normal">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    <li class="fs14 text-white mt-4 lh-lg fw-normal">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    <li class="fs14 text-white mt-4 lh-lg fw-normal">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>