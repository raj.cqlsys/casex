<?php include '../partials/head.php';
setPageTitle('Caes X Manager Law Frim');
?>
<?php include '../partials/navbarlogin.php'; ?>
<section class="py-5 bg_dark">
    <div class="container">
        <div class="row align-items-center gy-4 flex-wrap-reverse">
            <div class="col-lg-8">
                <h1 class="fs100 oswaldff mb-5">Modern Tools For The Personal Injury Industry</h1>
                <p class="fs24 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
            </div>
            <div class="col-lg-4 ps-lg-5 text-center"><img src="../assets/images/f1.png" alt="" style="max-height: 500px;" class="img-fluid"></div>
        </div>
    </div>
</section>

<section class="py-5 bg_black">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="fs90 oswaldff text_theme">Discover Our Suite Of Solutions</h2>
            </div>
        </div>
        <div class="row pt-lg-5 justify-content-between align-items-center  gy-4">
            <div class="col-lg-5">
                <img src="../assets/images/f2.png" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6">
                <h3 class="fs75 mb-4">Seamless Operations for Funders</h3>
                <p class="fs30 text_down_white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                </p>
            </div>
        </div>
    </div>
</section>

<hr>

<section class="py-5 bg_black">
    <div class="container">
        <div class="row pt-lg-5 justify-content-between align-items-center">
            <div class="col-lg-6">
                <img src="../assets/images/d1.png" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6">
                <h3 class="fs75 mb-4">Bill and Record Sharing for Medical Providers & Lawyers</h3>
                <p class="fs30 text_down_white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="py-5 bg_dark">
    <div class="container py-lg-4">

        <div class="swiper testi_slider">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <div class="text-center">
                                <p class="fs42 text-white  mb-4">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus dapibus leo.”</p>

                                <h4 class="fs46 text_theme">Gabi Rojas</h4>
                                <p class="fs24 mb-0 text_down_white">ChiroCare of Florida (Medical Provider)</p>
                                <p class="fs24 text_down_white">Miami, FL</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- slide end -->
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <div class="text-center">
                                <p class="fs42 text-white  mb-4">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus dapibus leo.”</p>

                                <h4 class="fs46 text_theme">Gabi Rojas</h4>
                                <p class="fs24 mb-0 text_down_white">ChiroCare of Florida (Medical Provider)</p>
                                <p class="fs24 text_down_white">Miami, FL</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- slide end -->
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <div class="text-center">
                                <p class="fs42 text-white  mb-4">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus dapibus leo.”</p>

                                <h4 class="fs46 text_theme">Gabi Rojas</h4>
                                <p class="fs24 mb-0 text_down_white">ChiroCare of Florida (Medical Provider)</p>
                                <p class="fs24 text_down_white">Miami, FL</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- slide end -->
            </div>
            <i class="ph ph-caret-left center_swiper_nav swiper_left_nav"></i>
            <i class="ph ph-caret-right center_swiper_nav swiper_right_nav"></i>
        </div>
    </div>
</section>

<section class="py-5">
    <div class="container py-lg-4">
        <div class="row">
            <div class="col-lg-6 text-center text-white mx-auto">
                <h3 class="fs32 under_wave">Contact Us <i class="ph ph-pulse under_wave_item"></i></h3>
                <p class="fs24">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar. Lorem ipsum dolor sit amet</p>
            </div>
        </div>
        <div class="row pt-5 gy-4">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="contact_card">
                    <div class="cont_ca_ico">
                        <i class="ph ph-map-pin"></i>
                    </div>
                    <div class="cont_ca_body">
                        <h4 class="fs24 text-white fw-semibold">LOCATION</h4>
                        <p class="fs24 text-white fw-normal mb-0">Lorem Ipsum is simply text.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <a href="mailto:info@demo.com" class="contact_card">
                    <div class="cont_ca_ico">
                        <i class="ph ph-envelope-simple"></i>
                    </div>
                    <div class="cont_ca_body">
                        <h4 class="fs24 text-white fw-semibold">EMAIL</h4>
                        <p class="fs24 text-white fw-normal mb-0">info@demo.com</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <a href="tel:+1 123-445-7777" class="contact_card">
                    <div class="cont_ca_ico">
                        <i class="ph ph-phone-call"></i>
                    </div>
                    <div class="cont_ca_body">
                        <h4 class="fs24 text-white fw-semibold">PHONE</h4>
                        <p class="fs24 text-white fw-normal mb-0">(+1) 123-445-7777</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="py-5 bg_dark">
    <div class="container py-lg-4">
        <div class="row">
            <div class="col-lg-7 col-md-10 mx-auto">
                <h4 class="fs36 text-white mb-5  text-center">Get In Touch</h4>
                <form action="">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Name</label>
                            <div class="inputGroup mb-3"><input type="text" name="" id="" class="inputControl" placeholder="Enter Name"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Email</label>
                            <div class="inputGroup mb-3"><input type="email" name="" id="" class="inputControl" placeholder="Enter Email"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Phone Number</label>
                            <div class="inputGroup mb-3"><input id="mobile_code" type="text" name="" id="" class="inputControl" placeholder="Enter Phone Number"></div>
                        </div>
                        <div class="col-md-6">
                            <label for="" class="fs18 mb-2 text-white">Subject</label>
                            <div class="inputGroup mb-3"><input type="email" name="" id="" class="inputControl" placeholder="Enter Subject"></div>
                        </div>
                        <div class="col-12">
                            <label for="" class="fs18 mb-2 text-white">Message</label>
                            <div class="inputGroup mb-3">
                                <textarea name="" id="" placeholder="Write here..." class="inputControl h-auto pt-4" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="theme_btn w-auto px-5 rounded-3 mx-auto mt-4">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include '../partials/footer.php'; ?>
<?php include '../partials/script.php'; ?>

<!-- ajax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js    "></script>
<script>
    // -----Country Code Selection
    $("#mobile_code").intlTelInput({
        initialCountry: "in",
        separateDialCode: true,
        // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
    });
</script>